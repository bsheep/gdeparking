class BaseUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick

  storage :file

  process :resize_and_pad => [600, 600, 'white']
  process :convert => 'jpg'
  process :strip

  def extension_white_list
    %w(jpg jpeg gif png JPG JPEG GIF PNG)
  end

  def store_dir
    "catalog/#{model.class.to_s.underscore}/#{model.id}"
  end

  def filename
    "#{secure_token(16)}.#{file.extension.downcase}" if original_filename.present?
  end


  def default_url
    "/no-image.jpg"
  end

  protected

  def strip
    manipulate! do |img|
      img.strip!
      img = yield(img) if block_given?
      img
    end
  end

  def secure_token(length=16)
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.hex(length/2))
  end

end
