# encoding: utf-8
class ParkingImageUploader < BaseUploader

  version :list_preview do
    process :resize_and_pad => [200, 200]
  end

  version :slider_big do
    # process :resize_and_pad => [600, 600]
    process :watermark
  end

  private

  def watermark(opacity = 0.55, size = '')
    manipulate! do |img|
      logo = Magick::Image.read("#{Rails.root}/app/assets/images/watermark#{size}.png").first
      img = img.composite(logo, Magick::CenterGravity, 10, 10, Magick::OverCompositeOp)
    end
  end

end
