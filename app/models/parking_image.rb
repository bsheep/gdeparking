class ParkingImage < ActiveRecord::Base
  include PositionSupporter

  acts_as_list scope: :parking
  belongs_to :parking, touch: true

  after_create :set_position

  mount_uploader :image, ParkingImageUploader

  def self.rebuild_all
    ParkingImage.all.each do |parking_image|
      if parking_image.parking.nil?
        parking_image.delete
        system("rm -rf #{Rails.root}/public/catalog/parking_image/#{parking_image.id}")
      else
        parking_image.image.recreate_versions!
        parking_image.save!
      end
    end
  end

end