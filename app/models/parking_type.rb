#encoding: utf-8
# Синяя	Официально бесплатная парковка
# Зеленая	Объекты, на которых можно свободно парковаться, т.к. платной парковки в округе нет
# Желтая	Объекты, на которых можно свободно парковаться, т.к. платной парковки в округе нет
# Красная	Популярное место для парковки, но противозаконное и с высокой вероятностью эвакуации
class ParkingType
  attr_accessor :name
  attr_accessor :code
  attr_accessor :description

  def self.collection
    [
        ParkingType.new(name: 'Синяя', description: 'Официально бесплатная парковка', code: 1),
        ParkingType.new(name: 'Зеленая', description: 'Объекты, на которых можно свободно парковаться, т.к. платной парковки в округе нет', code: 2),
        ParkingType.new(name: 'Желтая', description: 'Объекты, на которых можно свободно парковаться, т.к. платной парковки в округе нет', code: 3),
        ParkingType.new(name: 'Красная', description: 'Популярное место для парковки, но противозаконное и с высокой вероятностью эвакуации', code: 4)
    ]
  end

  def self.include?(code)
    collection.each do |role|
      return true if role.code == code
    end
    false
  end

  def self.code_by_name(name)
    collection.each do |value|
      return value.code if value.name == name
    end
    false
  end

  def self.by_code(code)
    collection.each do |value|
      return value.name if value.code == code
    end
    false
  end

  def initialize(hash)
    self.name = hash[:name]
    self.code = hash[:code]
    self.description = hash[:description]
  end

end