class Parking < ActiveRecord::Base
  default_scope { order('id ASC') }
  scope :active, -> { where(active: true) }
  has_many :parking_images, -> { order(:position) }, :dependent => :destroy, :inverse_of => :parking

  FILE_NAME = 'parking-full'
  ADDRESS = 'A'
  DESCRIPTION = 'B'
  AREA = 'D'
  PARK_TYPE = 'E'
  CAPACITY = 'F'
  LAT = 'H'
  LNG = 'I'
  LAT_ENTRY_1 = 'K'
  LNG_ENTRY_1 = 'L'
  LAT_ENTRY_2 = 'M'
  LNG_ENTRY_2 = 'N'
  INTERNAL_NUMBER = 'J'


  FIRST_LINE = 2

  SHEET_PARKINGS = 0
  SHEET_AREAS = 1
  SHEET_PARK_TYPE = 2

  def self.import_parking
    table_parkings = Roo::Excelx.new("public/#{FILE_NAME}.xlsx")
    sheet = table_parkings.sheets[SHEET_PARKINGS]

    FIRST_LINE.upto(table_parkings.last_row) do |line|
      parking_address = table_parkings.cell(line, ADDRESS, sheet) unless table_parkings.cell(line, ADDRESS, sheet).nil?
      parking_description = table_parkings.cell(line, DESCRIPTION, sheet) unless table_parkings.cell(line, DESCRIPTION, sheet).nil?
      parking_area = table_parkings.cell(line, AREA, sheet) unless table_parkings.cell(line, AREA, sheet).nil?
      parking_park_type = table_parkings.cell(line, PARK_TYPE, sheet) unless table_parkings.cell(line, PARK_TYPE, sheet).nil?
      parking_capacity = table_parkings.cell(line, CAPACITY, sheet) unless table_parkings.cell(line, CAPACITY, sheet).nil?
      parking_lat = table_parkings.cell(line, LAT, sheet) unless table_parkings.cell(line, LAT, sheet).nil?
      parking_lng = table_parkings.cell(line, LNG, sheet) unless table_parkings.cell(line, LNG, sheet).nil?
      parking_lat_entry_1 = table_parkings.cell(line, LAT_ENTRY_1, sheet) unless table_parkings.cell(line, LAT_ENTRY_1, sheet).nil?
      parking_lng_entry_1 = table_parkings.cell(line, LNG_ENTRY_1, sheet) unless table_parkings.cell(line, LNG_ENTRY_1, sheet).nil?
      parking_lat_entry_2 = table_parkings.cell(line, LAT_ENTRY_2, sheet) unless table_parkings.cell(line, LAT_ENTRY_2, sheet).nil?
      parking_lng_entry_2 = table_parkings.cell(line, LNG_ENTRY_2, sheet) unless table_parkings.cell(line, LNG_ENTRY_2, sheet).nil?
      parking_internal_number = table_parkings.cell(line, INTERNAL_NUMBER, sheet) unless table_parkings.cell(line, INTERNAL_NUMBER, sheet).nil?

      parking = Parking.create(address: parking_address,
                               description: parking_description,
                               area: Area.by_name(parking_area),
                               park_type: ParkingType.code_by_name(parking_park_type),
                               capacity: parking_capacity,
                               lat: parking_lat,
                               lng: parking_lng,
                               lat_entry_1: parking_lat_entry_1,
                               lng_entry_1: parking_lng_entry_1,
                               lat_entry_2: parking_lat_entry_2,
                               lng_entry_2: parking_lng_entry_2,
                               internal_number: parking_internal_number,
      )
      parking.save!
    end
  end

  def update_image_title(hash_image)
    unless hash_image
      return
    end
    hash_image.keys.each do |key|
      image = ParkingImage.find key
      if image.parking_id.nil?
        image = ParkingImage.find key
        image.parking_id = self.id
      end
      image.title = hash_image[key]
      image.save
    end
  end

end
