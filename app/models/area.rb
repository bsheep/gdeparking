#encoding: utf-8
class Area
  attr_accessor :name
  attr_accessor :code

  def self.collection
    [
        Area.new(:name => 'Арбат', :code => 1),
        Area.new(:name => 'Басманный район', :code => 2),
        Area.new(:name => 'Замоскворечье', :code => 3),
        Area.new(:name => 'Красносельский район', :code => 4),
        Area.new(:name => 'Мещанский район', :code => 5),
        Area.new(:name => 'Пресненский район', :code => 6),
        Area.new(:name => 'Таганский район', :code => 7),
        Area.new(:name => 'Тверской район', :code => 8),
        Area.new(:name => 'Хамовники', :code => 9),
        Area.new(:name => 'Якиманка', :code => 10)
    ]
  end

  def self.include?(code)
    collection.each do |role|
      return true if role.code == code
    end
    false
  end

  def self.by_code(code)
    collection.each do |value|
      return value.name if value.code == code
    end
    false
    end

  def self.by_name(name)
    collection.each do |value|
      return value.code if value.name == name
    end
    false
  end

  def initialize(hash)
    self.name = hash[:name]
    self.code = hash[:code]
  end

end