#encoding: utf-8
class FeedbackType
  attr_accessor :name
  attr_accessor :code

  def self.collection
    [
        FeedbackType.new(name: 'Android приложение', code: 1),
        FeedbackType.new(name: 'iOs приложение', code: 2),
        FeedbackType.new(name: 'Обращение', code: 3)
    ]
  end

  def self.include?(code)
    collection.each do |role|
      return true if role.code == code
    end
    false
  end

  def self.by_code(code)
    collection.each do |value|
      return value.name if value.code == code
    end
    false
  end

  def initialize(hash)
    self.name = hash[:name]
    self.code = hash[:code]
  end

end