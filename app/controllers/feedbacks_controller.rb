#encoding: utf-8
class FeedbacksController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  def create
    @feedback = Feedback.create(feedback_params)
    respond_to do |format|
      format.js
    end
  end

  def feedback_params
    params.require(:feedback).permit(:name, :email, :feedback_type, :comment, :mobile_phone)
  end

end
