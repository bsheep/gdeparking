class Administrator::ParkingsController < Administrator::CommonController

  def update
    super
    @parking.update_image_title params[:parking_image]
  end

  def process_active
    parking = Parking.find(params[:parking])
    parking.update_attribute(:active, params[:checked])
    parking.touch
    render :nothing => true, :status => 200
  end

  private

  def parking_params
    params.require(:parking).permit(:internal_number,
                                    :address,
                                    :description,
                                    :area,
                                    :park_type,
                                    :capacity,
                                    :active,
                                    :lat,
                                    :lng,
                                    :lat_entry_1,
                                    :lng_entry_1,
                                    :lat_entry_2,
                                    :lng_entry_2)
  end
end

