class Administrator::FeedbacksController < Administrator::CommonController

  private

    def feedback_params
      params.require(:feedback).permit(:name, :email, :feedback_type, :comment, :mobile_phone)
    end
end

