class Administrator::SortableController < ApplicationController

  def sort
    if params['SortElement']
      klass   = Object.const_get params[:obj]
      object  = klass.find(params[:SortElement][:id].to_i)
      object2 = klass.find(params[:SortElement][:id2].to_i)
      object.insert_at(if object2.position then object2.position else 0 end)
      object.save
      render :nothing => true, :status => 200
    end
  end

end