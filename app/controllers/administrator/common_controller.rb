class Administrator::CommonController < InheritedResources::Base

  before_filter :authenticate_user!
  before_action :check_administrator?

  layout 'administrator'

  #index, show, new, edit, create, update, destroy

  actions :all, :except => :show
  respond_to :html, only: [:index]
  respond_to :js, except: [:index]
  #respond_to :html, :xml, :json, :js

  # def create
  #   super do |format|
  #     format.html { redirect_to administrator_categories_path }
  #   end
  # end
  # # redirect to *index* instead of *show* by default
  # def create
  #   create! { collection_path }
  # end
  # def update
  #   update! { collection_path }
  # end
  # private
  # def i18n_name
  #   resource_class.name.underscore
  # end

  protected

  def check_administrator?
    redirect_to root_path unless current_user.admin?
  end

end