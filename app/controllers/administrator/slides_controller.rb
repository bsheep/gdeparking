class Administrator::SlidesController < Administrator::CommonController

  private

    def slide_params
      params.require(:slide).permit(:inside, :image, :position)
    end
end

