class Administrator::ParkingImagesController < ActionController::Base

  def create

    file    = params[:file].tempfile
    # klass   = Object.const_get params[:obj]

    # @object  = klass.create(parking_id: params[:parking_id])
    # @object.image     = params[:file]
    # @object.save!

    @image = ParkingImage.create(parking_id: params[:parking_id])
    @image.image     = params[:file]
    @image.save!

    path_file = params[:file].path
    file.close
    File.delete(path_file)

    render 'administrator/parkings/return_parking_image', image: @image

    rescue Exception => e
      render :text => "{error:#{e.message}}"
  end

  def destroy
    @image = ParkingImage.find(params[:parking_image_id])
    system("rm -rf #{Rails.root}/public/catalog/parking_image/#{@image.id}")
    @image.delete
  end

end