class Administrator::FeaturesController < Administrator::CommonController

  private

  def feature_params
    params.require(:feature).permit(:title, :description, :image, :position)
  end
end