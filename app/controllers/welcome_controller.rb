class WelcomeController < ApplicationController

  def cs
    render layout: "cs"
  end

  def index
    @slides = Slide.all
    @features = Feature.all
    @parkings = Parking.active
    @feedback = Feedback.new
  end

  def parking_images
    @parking_images = Parking.find(params['id']).parking_images.decorate
  end

end