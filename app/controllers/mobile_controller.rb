#encoding: utf-8
class MobileController < ApplicationController
  def parkings
    @parkings = Parking.active
    respond_to do |format|
      format.json
    end
  end
end
