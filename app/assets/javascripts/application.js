//= require jquery
//= require jquery_ujs
//= require foundation
// require frontend/swiper/swiper.js
//= require frontend/swiper/swiper.jquery
//= require frontend/owl/owl.carousel
//= require frontend/owl/owl.navigation
//= require frontend/owl/owl.lazyload
//= require frontend/owl/owl.autoplay
//= require frontend/owl/owl.video

//if ie9 require html5shiv
// require head.min
// require reveal

// Full list of configuration options available at:
// https://github.com/hakimel/reveal.js#configuration
//Reveal.initialize({
//    controls: true,
//    progress: true,
//    history: true,
//    center: true,
//
//    transition: 'slide',
//    // none/fade/slide/convex/concave/zoom
//
//    // Optional reveal.js plugins
//    dependencies: [
//        { src: 'classList.js', condition: function() { return !document.body.classList; } },
//        { src: '/assets/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
//        { src: '/assets/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
//        { src: '/assets/plugin/highlight/highlight.js', async: true, condition: function() { return !!document.querySelector( 'pre code' ); }, callback: function() { hljs.initHighlightingOnLoad(); } },
//        { src: '/assets/plugin/zoom-js/zoom.js', async: true },
//        { src: '/assets/plugin/notes/notes.js', async: true }
//    ]
//});
//
//$(function(){ $(document).foundation(); });
