var FORM_ID = '#administrator_modal';
var listSearch = null;
var queryStop = false;

function pasteProduct(data){

    var $dataItems = $('[data_items]');
    var strId = $(data).find('[id]');
    var $item = $($dataItems).find('#' + strId.attr('id'));
    if ( $item.length > 0 ) {
        var $counter = $($item).find('input#count_item');
        var count = $($counter).val();
        count = parseInt(count,10) + 1;
        $($counter).val(count);
        $($counter).trigger('change');
        $($item).addClass('line_red');
        setTimeout(function(){
            $($item).removeClass('line_red');
        }, 5000);
    } else {
        $($dataItems).append(data);
    }
}

function removeItem (item_id) {
    $('#administrator_modal div#cart_product_' + item_id).remove();
}

function queryResultFastSearch(product_id, order_id){
    $.ajax({
        type: "GET",
        url: "orders/add_product",
        data: "product[id]=" + product_id + "&order[id]=" + order_id
    });
}

function addInOrderProduct(id){
    var order_id = $(FORM_ID + ' form[data-order-id]').attr('data-order-id');
    queryResultFastSearch(id, order_id);
}

function fastSearch(elem){

    var input = elem;
    $(input).autocomplete({
        source:function(request, response){
            if ( !queryStop ) {
                $.ajax({
                    type: "GET",
                    url: "orders/order_fast_search.json",
                    data: "term=" + request.term,
                    success: function (data) {
                        listSearch = new Array();
                        if (data.products != null && data.products.length) {
                            for (i = 0; i < data.products.length; i++) {
                                listSearch[i] = { label:data.products[i].lable, value:data.products[i].value };
                            }
                        }
                        response(listSearch);
                        return;
                    }
                });
            } else {
                queryStop = false;
            }
        },
        minLength: 2,
        delay: 500,
        appendTo: FORM_ID,
        select: function(event, ui) {
            queryStop = true;
            $(input).blur();
            $(input).val(ui.item.label);
            addInOrderProduct(ui.item.value);
            return false;
        },
        focus: function(event, ui) {
           return false;
        }
    });

    $(input).focusin(function(){
        $(this).val('');
    })
}

function sumTotalCart(sum) {
    var total = sum.toString();
    var correctTotal = '';
    var outSum = '';

    for( var i = total.length - 1; i >= 0; i-- ) {
        if ( i == 7 || i == 4 ) {
            outSum += ' ' + total[total.length - 1 - i];
        } else {
            outSum += total[total.length - 1 - i];
        }
    }
    $('#sum_total').text(outSum);
}
