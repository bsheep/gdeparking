

var CONTROLLER_NAME = 'accessible_names_specification';
var FORM_ID = '#administrator_modal';
var BODY_EDIT_NAME = 'div[specification_block_name]';
var QUERY_PARAM_NEW_NAME = 'new_name_specifications';
var QUERY_PARAM_NEW_VALUE = 'new_value_specifications';
var LENGTH_NAME_PARAMETER = 3;
var EVENT_FOCUSOUT = 1;
var EVENT_KEY_ENTER = 2;

var $elemList = null;
var $elemLiable = null;
var $elemEditor = null;
var $deleteButton = null;
var $BlockRecordCurrent = null;
var listSearch = new Array();
var flagDeleteNewBlock = false;
var flagEndEdit = false;
var flagOneEventEdit = false;

String.prototype.replaceAll = function (search, replace) {
    return this.split(search).join(replace);
};

function getProductId() {
    return $('form input#product_id').val();
}

function deleteEvent(name) {
    $(name).unbind();
}

function deleteList(name) {
//    $(name).autocomplete('destroy');
}

function deleteEventDeleteSpecification(elem){
    deleteEvent(elem);
}

function deleteBlockSpecification(elem) {
    $(elem).remove();
}

function showEditor(elemInput) {
    $(elemInput).css('display', 'block');
    $(elemInput).prev().css('display', 'none');
    $(elemInput).focus();
}

function hideEditor(elemInput) {
    $(elemInput).css('display', 'none');
    $(elemInput).prev().css('display', 'block');
}

function closeEventEditor(elem) {
    deleteEvent(elem);
}

function closeForm() {
    setTimeout(function () {
        $('#click_administrator_modal_close').trigger('click');
        deleteEvent('[specification_name]');
    }, 1000);
}

function checkLengthName(elem) {
    var newName = new String($(elem).val());
    if ( newName.length >= LENGTH_NAME_PARAMETER ) return true;
    
    return false;
}

function checkNewBlockRecord(elem) {

    var name = new String( $(elem).attr('name') );
    if (name.search('new_parameter') > 0) return true;

    return false;
}

function checkName(str){

    var checkedName = new String(translit(str));
    var result = true;
    if ( checkedName.search(/^[0-9]/) >= 0) return false;
    if ( checkedName == 'brend' || checkedName == 'brand') return false;
    $('div#specification_list').find('span[specification_name]').each(function(){

        var name = new String(  $(this).text() );
        name = translit( name.replaceAll('\n','') );
        if( name.search(checkedName) != -1 ) {
            if(name.length == checkedName.length) result = false;
        }
    });
    return result;
}

function clearParameter() {

    $($BlockRecordCurrent).find('[specification_name]').text('');
    $($BlockRecordCurrent).find('[specification_editor_name]').attr('name', 'keys[no_rename][new_parameter]');
    $($BlockRecordCurrent).find('[specification_editor_name]').attr('value', '');
}

function newSpecification() {

    $BlockRecordCurrent = $("div#specification_list > div:first-child").clone();
    $deleteButton       = $($BlockRecordCurrent).find('i');
    $elemEditor         = $($BlockRecordCurrent).find(BODY_EDIT_NAME);

    $("div#specification_list").append($BlockRecordCurrent);

    clearParameter();
    initEventDeleteSpecification($deleteButton);
    initEventEdit($elemEditor);

    $($BlockRecordCurrent).css('display','block');
    $($elemEditor).click();
}

function noRename(elem){

    var paramName = new String($(elem).attr('name'));

    if (paramName.search('new_parameter') == -1 ) {
        $($elemLiable).text($($BlockRecordCurrent).attr('value'));
    }
}

function pasteListNames(elem, listSearch) {
    $(elem).autocomplete({
        source:listSearch,
        appendTo:FORM_ID
    });
}

function pasteNewName(elem) {

    var $lableElem  = $(elem).prev();
    var $input2     = $(elem).parent().next().find('input');
    var newName     = new String($(elem).val());
    var nameEng     = translit(newName);
    var paramName   = 'keys[new_name_specifications][#paste]';

    paramName  = paramName.replaceAll('#paste', nameEng);
    var paramValue  = paramName.replaceAll(QUERY_PARAM_NEW_NAME, QUERY_PARAM_NEW_VALUE);
    
    $($lableElem).text(newName);
    $($input2).attr('name', paramValue);
    $(elem).attr('name', paramName);
    $(elem).attr('value', newName);
}

function eventEditor (elem, type) {

    if (flagOneEventEdit) return false;
    if (checkLengthName(elem)) {
        if ( type == EVENT_FOCUSOUT ) {
            if (checkNewBlockRecord(elem)) {
                flagDeleteNewBlock = true;
            } else {
                noRename(elem);
                deleteEventDeleteSpecification($($BlockRecordCurrent).find('i'));
                hideEditor(elem);
                closeEventEditor(elem);
                deleteList(elem);
                flagOneEventEdit = true;
            }
        }
        if ( type == EVENT_KEY_ENTER ) {
            if ( checkName($(elem).val())) {
                pasteNewName(elem);
                hideEditor(elem);
                closeEventEditor(elem);
                deleteList(elem);
                flagOneEventEdit = true;
            } else {
                $(elem).val('');
                alert('Параметер существует или не соответствует требованию!(в начале не должна стоять цифра)');
                $(name).autocomplete('enable');
                $(elem).focus();
            }
        } 
    } else {
        if  ( type == EVENT_KEY_ENTER ) {
            if (!checkNewBlockRecord(elem)) {
                noRename(elem);
                hideEditor(elem);
                closeEventEditor(elem);
                deleteList(elem);
                alert('Название должно содержать больше 3 символов!');
            }
        }
        if (type == EVENT_FOCUSOUT ) {
            if (checkNewBlockRecord(elem)){
                flagDeleteNewBlock = true;
            } else {
                noRename(elem);
                hideEditor(elem);
                closeEventEditor(elem);
                deleteList(elem);
                flagOneEventEdit = true;
            }
        }
    }
    if ( flagDeleteNewBlock ) {
        hideEditor(elem);
        deleteEventDeleteSpecification($deleteButton);
        deleteBlockSpecification($BlockRecordCurrent);
        flagDeleteNewBlock = false;
        flagOneEventEdit = true;
    }
    return false;
}

function createEditor(elem) {

    var $lableElem  = $(elem).children('span');
    var $editor     = $(elem).children('input');
    var name        = $($lableElem).text();
    
    name = name.replaceAll("\n", '').replaceAll("  ", '');
    $($editor).val(name);

    $BlockRecordCurrent = $(elem).closest('.row');
    $elemEditor         = $editor;
    $elemLiable         = $lableElem;
    $elemList           = $($editor).next();
    $deleteButton       = $($BlockRecordCurrent).find('i');

    queryAccessibleListNames(getProductId(), CONTROLLER_NAME);
    offKeyEnter($(FORM_ID).find('input'));
    showEditor($editor);

    flagOneEventEdit = false;

    $($editor).mouseleave( function(){
        flagEndEdit = true;
    });
    $($editor).mouseenter( function(){
        flagEndEdit = false;
    });
    $('body').mousedown(function(){
       if ( flagEndEdit ) eventEditor($elemEditor, EVENT_FOCUSOUT);
    });
    $($editor).keydown(function(){
        if(event.keyCode == 13) eventEditor(this, EVENT_KEY_ENTER);
    });
}

function queryAccessibleListNames(productId, controller) {

    $.ajax({
        type: "GET",
        url: "/administrator/products/" + controller + ".json",
        data: "product[id]=" + productId,
        success: function (data) {
            listSearch = new Array();
            if (data.names != null && data.names.length) {
                for (i = 0; i < data.names.length; i++) {
                    listSearch[i] = {value:data.names[i].name};
                }
                pasteListNames($elemEditor, listSearch);
            }
        }
    });
}

function queryDeleteSpecification(elem) {

    $(elem).unbind();
    var $rowDelete = $(elem).closest('.row');
    $.ajax({
        type: "GET",
        url: "/administrator/products/delete_specification",
        data: "product[id]=" + getProductId() + "&delete_specification=" + $(elem).attr('delete_this_specification'),
        success: function (data) {
            $($rowDelete).remove();
        }
    });
}

function initTextEditor() {
    $('#product_description, #product_short_description, #product_application').redactor({
        lang: 'ru',
        buttons: ['html', 'formatting', 'bold', 'unorderedlist'],
        cleanSpaces: true
    });
}

function initEventEdit(name) {
    $(name).bind('click', function () {
        createEditor(this);
    });
}

function initEventDeleteSpecification(elem) {
    $(elem).bind('click', function () {
        queryDeleteSpecification(this);
    });
}

function searchNoSpecification() {

    $('div#specification_list').find('div.row').each(function(){

        var $input  = $(this).find('input[specification_editor_name]');
        var name    =$($input).attr('name');

        if (name.search('brend') != -1 || name.search('brand') != -1) $(this).remove();
    });
}

function openForm(dataHtml) {

    deleteEvent(BODY_EDIT_NAME);
    $(FORM_ID).html(dataHtml);
    searchNoSpecification();
    initEventEdit(BODY_EDIT_NAME);
    initEventDeleteSpecification('i[delete_this_specification]');
    offKeyEnter($(FORM_ID).find('input'));
    $('#click_administrator_modal').trigger('click');
    animatedSave('#save');
    $elemEditor = null;
    $elemList = null;
    $BlockRecordCurrent = null;
}

function setPriceProduct(elem, id){

    var $thisInput = $(elem);
    $.ajax({
        type: "GET",
        url: "/administrator/products/set_price",
        data: "product[id]=" + id + "&product[price]=" + $($thisInput).val()
    });
}

function setPriceProductOrder(elem, order_id, product_id) {

    var $thisInput = $(elem);
    $.ajax({
        type: "GET",
        url: "orders/set_price_product",
        data: "product[id]=" + product_id + "&product[price]=" + $($thisInput).val() + "&order[id]=" + order_id
    });
}

function setProductCount(elem, order, productId) {
    productCount = $(elem).val();
    $.get("/administrator/orders/set_count?product_id=" + productId + "&count=" + productCount + '&order[order_id]='+ order);
}

function addVideo() {
    $('div#list-videos').prepend('<div class="row" data-video-content><div class="large-11 columns">Url<input name="product_video[' + Math.floor(Math.random() * (1000000 - 1 + 1)) + 1 +
    ']" name="product_video" type="text" value="" tabindex="0"></div><div class="large-1 columns ad_top_pad" style="padding:30px;"><a href="#" tabindex="0" onclick="delVideo(this);"><i tabindex="0" class="fa fa-remove 2x"></i></a></div></div></div>');
}

function delVideo(elem) {
    $(elem).closest('div.row').remove();
}

function animatedSave(elem) {

    $(elem).on('ajax:before', function(xhr){
        $(this).html('<a href="#" class="button disabled"><i class="fa fa-spinner fa-pulse"></i></a>');
    });

    $(elem).on('ajax:success', function(event, data){
        //var elem     = this;
        //var saveData = data;
        setTimeout(function(){
            //$(elem).html('В корзине');
            //$(elem).removeClass('alert');
            //$(elem).removeClass('add-to-cart');
            //$(elem).addClass('secondary');
            //$(elem).addClass('in-cart');
            //$(elem).blur();
        }, 500);
    });
}