
$(function () {

    String.prototype.replaceAll = function (search, replace) {
        return this.split(search).join(replace);
    };

    $(document).ready(function () {

        var strQuery = new Array();
        var controller = "/administrator/sortable/sort";
        var activeElement = new Array();

        var sortAjax = function (dataQuery) {
            $.ajax(controller, options = {
                type: 'POST', data: dataQuery
            });
        };

        function ElementSortable() {

            this.$elem        = 0;
            this.$elemetes    = 0;
            this.id           = 0;
            this.idTwo        = 0;
            this.position     = 0;
            this.objClass     = '';

            this.getIndexOfId = function (id) {

                for (var i = 0; i < this.$elemetes.length; i++) {
                    var elemId = $(this.$elemetes[i]).attr("data-model-id");
                    if (id == elemId) {
                        return i;
                    }
                }
                return -1;
            };

            function getIdElement($elem) {
                return $($elem).attr("data-model-id");
            }

            function getTypeObj($elem) {
                return $($elem).attr('data-obj')
            }

            this.init = function ($objElem, $objArraElemetes) {

                this.$elem     = $objElem;
                this.$elemetes = $objArraElemetes;
                this.id        = getIdElement(this.$elem);
                this.objClass  = getTypeObj(this.$elem);
                this.position  = this.getIndexOfId(this.id);
            };
        }

        window.InitSortable = function(elemStr, elem) {

            var item = elem ? elem :'> li';

            $(elemStr).sortable({
                deactivate: function(event, ui) {

                    var $elem         = ui.item;
                    var $listProducts = ui.sender.context.children;
                    var elem          = new ElementSortable();
                    var elemTwo       = new ElementSortable();

                    elem.init($elem, $listProducts);

                    if ( activeElement[0] == elem.position && activeElement[1] == elem.id) {
                        return;
                    } else {
                        if ( activeElement[0] > elem.position) {
                            elemTwo.init($listProducts[elem.position + 1], $listProducts)
                        } else {
                            elemTwo.init($listProducts[elem.position - 1], $listProducts)
                        }
                    }
                    strQuery[0] = { name:'SortElement[id]',  value: elem.id };
                    strQuery[1] = { name:'SortElement[id2]', value: elemTwo.id};
                    strQuery[2] = { name:'obj', value: elem.objClass};
                    sortAjax( strQuery );
                },

                activate: function(event, ui) {
                    var $elem         = ui.item;
                    var $listProducts = ui.sender.context.children;
                    var elem          = new ElementSortable();

                    elem.init($elem, $listProducts);

                    activeElement[0] = elem.position;
                    activeElement[1] = elem.id;
                },

                items: item
            });
        };
    });

});