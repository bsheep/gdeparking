/**
 * Created by afarforov on 02/12/14.
 */

String.prototype.replaceAll = function (search, replace) {
    return this.split(search).join(replace);
};

function offKeyEnter(elem) {
    $(elem).keydown(function(){
        if(event.keyCode == 13 || event.keyCode == 27) {
            $(this).blur();
            return this;
        }
    });
}

function products_check_all(elem) {

    var $parentInput = $(elem).closest('ul');
    $($parentInput).next('ul').each(function () {
        $(this).find('.checked-all-process').each(function(){
            this.checked = elem.checked;
        });
    });
}

function processParkingActive(elem) {

    var dataQuery = new Array();

    dataQuery[0] = {name:'parking', value: elem.value };
    dataQuery[1] = {name: 'checked', value: elem.checked};

    $.getJSON('/administrator/parkings/process_active', jQuery.param(dataQuery));
}

function activateBlock(category_id) {

    var $link = $("#link_category" + category_id);
    openBlock = true || openBlock;

    $($link).attr('href', "#content_category" + category_id);
    $($link).removeAttr('data-remote');
    $($link).click();
}

function clearProductsList(category_id) {
    var elemDel = $("#content_category" + category_id).find('form[products_category=' + category_id + ']');
    if (elemDel.length > 0) {
        elemDel.remove();
        return false;
    }
    return true;
}

function pasteProductsList(category_id, dataHtml) {

    if (dataHtml == null || dataHtml == 0 || dataHtml.size == 0) return;

    if ( clearProductsList(category_id) ) {
        activateBlock(category_id);
    }
    $("#content_category" + category_id).append(dataHtml);
    offKeyEnter('input');
    InitSortable("ul.sortable");
}

function updateProductsList(category_id, controller) {

    var $blockCategory = $('dd#block_category' + category_id);
    var $link = $("#link_category" + category_id);

    if ($blockCategory.size == 0) return;

    $($link).attr('data-remote', 'true');

    if ($($blockCategory).hasClass('active')) {

        $($blockCategory).css('min-height',$($blockCategory).css('height'));
        $($blockCategory).find('div.content').css('display','none');
        $.ajax({
            url: controller,
            success: function (data) {
                setTimeout(function(){
                    $($blockCategory).find('div.content').css('display','');
                    $($blockCategory).css('min-height','');
                }, 2000);
            },
            error: function (data) {
                $($blockCategory).find('div.content').css('display', '');
                $($blockCategory).css('min-height', '');
            }
        });
    } else {
        $($link).attr('href', controller + '.js');
        clearProductsList(category_id);
    }
}

function deleteProductsFromList(product_id) {

    var regular = /\s*,\s*/;
    var formated_string = product_id.replaceAll('&quot;','').replaceAll('[','').replaceAll(']','');
    var tagList = formated_string.split(regular);

    for (i = 0; tagList.length > i; i++) {
        $("form ul li[data-model-id =" + tagList[i] + "]").hide(200,function(){
            $(this).remove();
        });
    }
}

function deleteProduct(product_id) {

   $("form ul li[data-model-id =" + product_id + "]").hide(200,function(){
       $(this).remove();
   });
}

function re_count_products(data) {

    if (data == null || data.category.length == 0) return 0;

    for (i = 0; data.category.length > i; i++) {
        var nameSearch = 'dd a#link_category' + data.category[i].id;
        $(nameSearch).text(data.category[i].name + ' (' + data.category[i].count + ')');
    }
}

