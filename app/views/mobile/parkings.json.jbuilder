json.parkings @parkings do |parking|
  json.id parking.id
  json.address parking.address
  json.description parking.description
  json.area Area.by_code(parking.area)
  json.park_type parking.park_type
  json.capacity parking.capacity
  json.lat parking.lat
  json.lng parking.lng
  json.lat_entry_1 parking.lat_entry_1
  json.lng_entry_1 parking.lng_entry_1
  json.lat_entry_2 parking.lat_entry_2
  json.lng_entry_2 parking.lng_entry_2
  json.parking_images parking.parking_images do |parking_image|
    json.title parking_image.title
    json.image parking_image.image_url(:slider_big)
  end
end