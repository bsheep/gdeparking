class ParkingImageDecorator < Draper::Decorator
  delegate_all
  def slide
    h.image_tag image_url(:slider_big)  , alt: title_or_address,title: title_or_address
  end

  private

  def title_or_address
    return Parking.find(parking_id).address if title.empty?
    title
  end
  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

end