class CreateParkingImages < ActiveRecord::Migration
  def change
    create_table :parking_images do |t|
      t.string :title
      t.integer :parking_id
      t.integer :position
      t.string :image

      t.timestamps
    end
  end
end
