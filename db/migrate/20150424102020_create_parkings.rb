class CreateParkings < ActiveRecord::Migration
  def change
    create_table :parkings do |t|
      t.string :address
      t.text :description
      t.integer :area
      t.integer :park_type
      t.integer :capacity
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
