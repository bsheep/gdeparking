class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :name
      t.string :email
      t.integer :feedback_type
      t.text :comment

      t.timestamps
    end
  end
end
