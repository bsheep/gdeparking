class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.string :title
      t.text :description
      t.string :image
      t.integer :position

      t.timestamps
    end
  end
end
