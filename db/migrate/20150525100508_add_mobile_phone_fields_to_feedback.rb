class AddMobilePhoneFieldsToFeedback < ActiveRecord::Migration
  def change
    add_column :feedbacks, :mobile_phone, :string
  end
end
