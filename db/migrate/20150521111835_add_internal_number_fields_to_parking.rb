class AddInternalNumberFieldsToParking < ActiveRecord::Migration
  def change
    add_column :parkings, :internal_number, :string
  end
end
