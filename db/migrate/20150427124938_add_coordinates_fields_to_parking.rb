class AddCoordinatesFieldsToParking < ActiveRecord::Migration
  def change
    add_column :parkings, :lat, :float
    add_column :parkings, :lng, :float
  end
end
