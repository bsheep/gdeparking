class AddCoordinatesEntryFieldsToParking < ActiveRecord::Migration
  def change
    add_column :parkings, :lat_entry_1, :float
    add_column :parkings, :lng_entry_1, :float
    add_column :parkings, :lat_entry_2, :float
    add_column :parkings, :lng_entry_2, :float
  end
end
