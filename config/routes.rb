Rails.application.routes.draw do
  devise_for :users
  get 'parking_images/:id' => 'welcome#parking_images', as: :parking_images
  get 'mobile/parkings' => 'mobile#parkings'
  get 'feedback' => 'welcome#feedback', as: :feedback_modal

  resources :feedbacks, :only => [:create]

  namespace :administrator do
    resources :features, except: :show
    resources :parkings, :except => :show do
      # member do
      #   get :clone
      # end
      collection do
        get :process_active
        # post :process_checked_list
        # post :sort
        # get :set_price
      end
    end
    resources :sortable do
      collection do
        post :sort
      end
    end
    resources :slides, except: :show
    resources :feedbacks, except: :show
    resources :parking_images, :only => [:create] do
      get '/parking_image/destroy' => 'parking_images#destroy'
    end
    root 'parkings#index'
  end

  # get 'welcome' => 'welcome#index'
  # root 'welcome#cs'
  root 'welcome#index'
end
